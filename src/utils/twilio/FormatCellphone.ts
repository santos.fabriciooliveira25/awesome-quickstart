export default function formatCellphone(cellphone: string): string {
	return `+55${cellphone.replace(/[^0-9]+/gi, "")}`;
}
