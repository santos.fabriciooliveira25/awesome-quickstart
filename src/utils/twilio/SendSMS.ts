import { Twilio } from "twilio";

import formatCellphone from "./FormatCellphone";

export default function SendSMS(cellphone: string, body: string): void {
	const accountSid = process.env.TWILIO_ACCOUNT_SID;
	const authToken = process.env.TWILIO_AUTH_TOKEN;
	const twilioNumber = process.env.TWILIO_PHONE_NUMBER;
	const formattedNumber = formatCellphone(cellphone);

	if (accountSid && authToken && twilioNumber) {
		const client = new Twilio(accountSid, authToken);

		client.messages
			.create({
				from: twilioNumber,
				to: formattedNumber,
				body,
			})
			.then((message) => console.log({ twilio: message.sid }));
	} else {
		console.error({
			twilio: "Some variable is missing that you need to send a message",
		});
	}
}
