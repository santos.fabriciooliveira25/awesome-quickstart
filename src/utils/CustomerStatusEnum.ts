export enum CustomerStatusEnum {
	PENDING = "pending",
	LEAD = "lead",
	SIMULATOR = "simulator",
	DONE = "done",
}
