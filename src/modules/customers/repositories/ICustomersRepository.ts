import { ICreateCustomerDTO } from "@modules/customers/dtos/ICreateCustomerDTO";
import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";

import { IUpdateCustomerDTO } from "../dtos/IUpdateCustomerDTO";

interface ICustomersRepository {
	create(data: ICreateCustomerDTO): Promise<Customer>;
	update(data: IUpdateCustomerDTO): Promise<Customer>;
	findByCpfOrEmail(email?: string, cpf?: string): Promise<Customer | undefined>;
	findById(id: string): Promise<Customer | undefined>;
}

export { ICustomersRepository, ICreateCustomerDTO };
