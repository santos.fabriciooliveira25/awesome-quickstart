import { ICreateCustomerGameDTO } from "../dtos/ICreateCustomerGameDTO";
import { ICustomerGamesResumeDTO } from "../dtos/ICustomerGamesResumeDTO";
import { Game } from "../infra/typeorm/entities/Game";

interface ICustomerGamesRepository {
	create(data: ICreateCustomerGameDTO): Promise<Game>;
	findGamesByCustomerId(customer_id: string): Promise<Game[]>;
	customerGamesResume(customer_id: string): Promise<ICustomerGamesResumeDTO>;
}

export { ICustomerGamesRepository };
