import { ICreateCustomerParentDTO } from "../dtos/ICreateCustomerParentDTO";
import { IDeleteCustomerParentDTO } from "../dtos/IDeleteCustomerParentDTO";
import { IUpdateCustomerParentDTO } from "../dtos/IUpdateCustomerParentDTO";
import { Parent } from "../infra/typeorm/entities/Parent";

interface ICustomersParentRepository {
	create(data: ICreateCustomerParentDTO): Promise<Parent>;
	update(data: IUpdateCustomerParentDTO): Promise<Parent>;
	delete(data: IDeleteCustomerParentDTO): Promise<void>;
}

export { ICustomersParentRepository };
