interface IUpdateCustomerParentDTO {
	id: string;
	fullname: string;
	cpf: string;
	email: string;
	birthdate: string;
	cellphone: string;
	cep: string;
}

export { IUpdateCustomerParentDTO };
