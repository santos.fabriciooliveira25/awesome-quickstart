interface ICustomerGamesResumeDTO {
	total_games: number;
	total_playtime: number;
}
export { ICustomerGamesResumeDTO };
