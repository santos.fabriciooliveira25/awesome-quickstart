interface ICreateCustomerGameDTO {
	name: string;
	playtime: number;
	background_image: string;
	store_name: string;
	store_id: string;
	store_slug: string;
	customer_id: string;
}

export { ICreateCustomerGameDTO };
