import { Parent } from "../infra/typeorm/entities/Parent";

interface ICreateCustomerDTO {
	fullname: string;
	cpf?: string;
	email: string;
	birthdate?: string;
	cellphone: string;
	cep?: string;
	status?: string;
	parent?: Parent;
}

export { ICreateCustomerDTO };
