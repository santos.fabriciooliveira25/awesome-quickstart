import { Game } from "../infra/typeorm/entities/Game";
import { Parent } from "../infra/typeorm/entities/Parent";

interface IUpdateCustomerDTO {
	id: string;
	fullname: string;
	cpf?: string;
	email: string;
	birthdate?: string;
	cellphone: string;
	cep?: string;
	status?: string;
	parent?: Parent;
	games?: Game[];
}

export { IUpdateCustomerDTO };
