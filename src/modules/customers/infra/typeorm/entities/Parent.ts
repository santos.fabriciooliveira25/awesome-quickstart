import {
	Column,
	CreateDateColumn,
	Entity,
	PrimaryColumn,
	UpdateDateColumn,
} from "typeorm";
import { v4 as uuidV4 } from "uuid";

@Entity("customers_parent")
class Parent {
	@PrimaryColumn()
	id: string;

	@Column()
	fullname: string;

	@Column()
	cpf: string;

	@Column()
	email: string;

	@Column({ type: "date" })
	birthdate: string;

	@Column()
	cellphone: string;

	@Column()
	cep: string;

	@CreateDateColumn({ select: false })
	created_at: Date;

	@UpdateDateColumn({ select: false })
	updated_at: Date;

	constructor() {
		if (!this.id) {
			this.id = uuidV4();
		}
	}
}

export { Parent };
