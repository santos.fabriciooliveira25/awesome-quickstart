import {
	Column,
	CreateDateColumn,
	Entity,
	PrimaryColumn,
	UpdateDateColumn,
} from "typeorm";
import { v4 as uuidV4 } from "uuid";

@Entity("customer_games")
class Game {
	@PrimaryColumn()
	id: string;

	@Column()
	name: string;

	@Column()
	playtime: number;

	@Column({ nullable: true })
	background_image: string;

	@Column({ nullable: true })
	store_id: string;

	@Column()
	store_name: string;

	@Column({ nullable: true })
	store_slug: string;

	@Column()
	customer_id: string;

	@CreateDateColumn({ select: false })
	created_at: Date;

	@UpdateDateColumn({ select: false })
	updated_at: Date;

	constructor() {
		if (!this.id) {
			this.id = uuidV4();
		}
	}
}

export { Game };
