import {
	// BaseEntity,
	Column,
	CreateDateColumn,
	Entity,
	JoinColumn,
	OneToOne,
	PrimaryColumn,
	UpdateDateColumn,
} from "typeorm";
import { v4 as uuidV4 } from "uuid";

import { Parent } from "./Parent";

@Entity("customers")
class Customer {
	@PrimaryColumn()
	id: string;

	@Column()
	fullname: string;

	@Column({ nullable: true })
	cpf: string;

	@Column()
	email: string;

	@Column({ type: "date", nullable: true })
	birthdate: string;

	@Column()
	cellphone: string;

	@Column({ nullable: true })
	cep: string;

	@Column()
	status: string;

	@OneToOne(() => Parent)
	@JoinColumn({ name: "parent_id" })
	parent: Parent;

	@CreateDateColumn({ select: false })
	created_at: Date;

	@UpdateDateColumn({ select: false })
	updated_at: Date;

	constructor() {
		if (!this.id) {
			this.id = uuidV4();
		}
	}
}

export { Customer };
