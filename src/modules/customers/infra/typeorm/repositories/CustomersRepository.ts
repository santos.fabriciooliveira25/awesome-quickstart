import { getRepository, Repository } from "typeorm";

import { ICreateCustomerDTO } from "@modules/customers/dtos/ICreateCustomerDTO";
import { IUpdateCustomerDTO } from "@modules/customers/dtos/IUpdateCustomerDTO";
import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";

class CustomersRepository implements ICustomersRepository {
	private repository: Repository<Customer>;

	constructor() {
		this.repository = getRepository(Customer);
	}

	async create({
		fullname,
		cpf,
		email,
		birthdate,
		cellphone,
		cep,
		status,
		parent,
	}: ICreateCustomerDTO): Promise<Customer> {
		const customer = this.repository.create({
			fullname,
			cpf,
			email,
			birthdate,
			cellphone,
			cep,
			status,
			parent,
		});

		await this.repository.save(customer);

		return customer;
	}

	async update({
		id,
		fullname,
		cpf,
		email,
		birthdate,
		cellphone,
		cep,
		status,
		parent,
		games,
	}: IUpdateCustomerDTO): Promise<Customer> {
		const customer = this.repository.save({
			id,
			fullname,
			cpf,
			email,
			birthdate,
			cellphone,
			cep,
			status,
			parent,
			games,
		});

		return customer;
	}

	async findByCpfOrEmail(
		email: string,
		cpf: string
	): Promise<Customer | undefined> {
		const customer = await this.repository.findOne({
			relations: ["parent"],
			where: [{ email }, { cpf }],
		});
		return customer;
	}

	async findById(id: string): Promise<Customer | undefined> {
		const customer = await this.repository.findOne(
			{ id },
			{ relations: ["parent"] }
		);
		return customer;
	}
}

export { CustomersRepository };
