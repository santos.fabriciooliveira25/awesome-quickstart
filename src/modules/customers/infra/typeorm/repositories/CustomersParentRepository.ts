import { getRepository, Repository } from "typeorm";

import { ICreateCustomerParentDTO } from "@modules/customers/dtos/ICreateCustomerParentDTO";
import { IDeleteCustomerParentDTO } from "@modules/customers/dtos/IDeleteCustomerParentDTO";
import { IUpdateCustomerParentDTO } from "@modules/customers/dtos/IUpdateCustomerParentDTO";
import { Parent } from "@modules/customers/infra/typeorm/entities/Parent";
import { ICustomersParentRepository } from "@modules/customers/repositories/ICustomersParentRepository";

class CustomersParentRepository implements ICustomersParentRepository {
	private repository: Repository<Parent>;

	constructor() {
		this.repository = getRepository(Parent);
	}

	async create({
		fullname,
		cpf,
		email,
		birthdate,
		cellphone,
		cep,
	}: ICreateCustomerParentDTO): Promise<Parent> {
		const parent = this.repository.create({
			fullname,
			cpf,
			email,
			birthdate,
			cellphone,
			cep,
		});

		await this.repository.save(parent);

		return parent;
	}

	async update({
		id,
		fullname,
		cpf,
		email,
		birthdate,
		cellphone,
		cep,
	}: IUpdateCustomerParentDTO): Promise<Parent> {
		const parent = this.repository.create({
			id,
			fullname,
			cpf,
			email,
			birthdate,
			cellphone,
			cep,
		});

		await this.repository.save(parent);

		return parent;
	}

	async delete(data: IDeleteCustomerParentDTO): Promise<void> {
		await this.repository.delete({ id: data.id });
	}
}

export { CustomersParentRepository };
