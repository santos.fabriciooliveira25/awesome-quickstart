import { getRepository, Repository } from "typeorm";

import { ICreateCustomerGameDTO } from "@modules/customers/dtos/ICreateCustomerGameDTO";
import { ICustomerGamesResumeDTO } from "@modules/customers/dtos/ICustomerGamesResumeDTO";
import { ICustomerGamesRepository } from "@modules/customers/repositories/ICustomerGamesRepository";

import { Game } from "../entities/Game";

class CustomerGamesRepository implements ICustomerGamesRepository {
	private repository: Repository<Game>;

	constructor() {
		this.repository = getRepository(Game);
	}

	async create({
		name,
		playtime,
		background_image,
		store_name,
		store_id,
		store_slug,
		customer_id,
	}: ICreateCustomerGameDTO): Promise<Game> {
		const customerGame = this.repository.create({
			name,
			playtime,
			background_image,
			store_name,
			store_id,
			store_slug,
			customer_id,
		});

		await this.repository.save(customerGame);

		return customerGame;
	}

	findGamesByCustomerId(customer_id: string): Promise<Game[]> {
		const games = this.repository.find({ customer_id });

		return games;
	}

	customerGamesResume(customer_id: string): Promise<ICustomerGamesResumeDTO> {
		const resume = this.repository
			.createQueryBuilder("customer_games")
			.select("COUNT(*)", "total_games")
			.addSelect("SUM(playtime)", "total_playtime")
			.where("customer_games.customer_id = customer_id", { customer_id })
			.getRawOne();

		return resume;
	}
}

export { CustomerGamesRepository };
