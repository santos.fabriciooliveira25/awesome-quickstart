import { inject, injectable } from "tsyringe";

import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";
import { ApiError } from "@shared/errors/ApiError";
import { CustomerStatusEnum } from "@utils/CustomerStatusEnum";
import SendSMS from "@utils/twilio/SendSMS";

interface IRequest {
	fullname: string;
	email: string;
	cellphone: string;
}

@injectable()
class InviteCustomerUseCase {
	constructor(
		@inject("CustomersRepository")
		private customersRepository: ICustomersRepository
	) {}

	async execute({ fullname, email, cellphone }: IRequest): Promise<void> {
		const customerAlreadyExists = await this.customersRepository.findByCpfOrEmail(
			email
		);

		if (customerAlreadyExists)
			throw new ApiError("Customer already exists", 409);

		await this.customersRepository.create({
			fullname,
			email,
			cellphone,
			status: CustomerStatusEnum.PENDING,
		});

		SendSMS(cellphone, "Teste");
	}
}

export { InviteCustomerUseCase };
