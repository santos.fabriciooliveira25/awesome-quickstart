import { Request, Response } from "express";
import { container } from "tsyringe";

import { InviteCustomerUseCase } from "@modules/customers/useCases/inviteCustomer/InviteCustomerUseCase";
import { connect } from "@shared/infra/typeorm";

class InviteCustomerController {
	async handle(request: Request, response: Response): Promise<Response> {
		await connect();

		const { fullname, email, cellphone } = request.body;

		const createCustomerUseCase = container.resolve(InviteCustomerUseCase);

		await createCustomerUseCase.execute({
			fullname,
			email,
			cellphone,
		});

		return response.status(201).send();
	}
}

export { InviteCustomerController };
