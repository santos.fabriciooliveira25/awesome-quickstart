import { Request, Response } from "express";
import { container } from "tsyringe";

import { UpdateCustomerUseCase } from "@modules/customers/useCases/updateCustomer/UpdateCustomerUseCase";
import { connect } from "@shared/infra/typeorm";

class UpdateCustomerController {
	async handle(request: Request, response: Response): Promise<Response> {
		await connect();

		const { id } = request.params;
		const {
			fullname,
			cpf,
			email,
			birthdate,
			cellphone,
			cep,
			parent,
			status,
		} = request.body;

		const updateCustomerUseCase = container.resolve(UpdateCustomerUseCase);

		const customer = await updateCustomerUseCase.execute({
			id,
			fullname,
			cpf,
			email,
			birthdate,
			cellphone,
			cep,
			parent,
			status,
		});

		return response.json(customer);
	}
}

export { UpdateCustomerController };
