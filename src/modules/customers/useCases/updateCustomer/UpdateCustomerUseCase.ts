import { inject, injectable } from "tsyringe";

import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";
import { Parent } from "@modules/customers/infra/typeorm/entities/Parent";
import { ICustomersParentRepository } from "@modules/customers/repositories/ICustomersParentRepository";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";
import { ApiError } from "@shared/errors/ApiError";

interface IRequest {
	id: string;
	fullname: string;
	cpf: string;
	email: string;
	birthdate: string;
	cellphone: string;
	cep: string;
	parent: Parent;
	status: string;
}

@injectable()
class UpdateCustomerUseCase {
	constructor(
		@inject("CustomersRepository")
		private customersRepository: ICustomersRepository,
		@inject("CustomersParentRepository")
		private customerParentRepository: ICustomersParentRepository
	) {}

	async execute({
		id,
		fullname,
		cpf,
		email,
		birthdate,
		cellphone,
		cep,
		parent,
		status,
	}: IRequest): Promise<Customer | undefined> {
		const customerExists = await this.customersRepository.findById(id);

		if (!customerExists) throw new ApiError("Customer not exists", 404);

		if (parent) {
			if (!parent.id || parent.id !== customerExists.parent.id)
				throw new ApiError("Parent id is missing or invalid", 400);
			await this.customerParentRepository.update({
				id: parent.id,
				fullname: parent.fullname,
				cpf: parent.cpf,
				email: parent.email,
				birthdate: parent.birthdate,
				cellphone: parent.cellphone,
				cep: parent.cep,
			});
		}

		try {
			const updatedCustomer = await this.customersRepository.update({
				id,
				fullname,
				cpf,
				email,
				birthdate,
				cellphone,
				cep,
				status,
			});

			return await this.customersRepository.findById(updatedCustomer.id);
		} catch (e) {
			throw new ApiError("Error creating customer", 400);
		}
	}
}

export { UpdateCustomerUseCase };
