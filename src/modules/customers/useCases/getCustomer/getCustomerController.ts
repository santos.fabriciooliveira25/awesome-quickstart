import { Request, Response } from "express";
import { container } from "tsyringe";

import { GetCustomerUseCase } from "@modules/customers/useCases/getCustomer/getCustomerUseCase";
import { connect } from "@shared/infra/typeorm";

class GetCustomerController {
	async handle(request: Request, response: Response): Promise<Response> {
		await connect();

		const { email, cpf } = request.query;

		const getCustomerUseCase = container.resolve(GetCustomerUseCase);

		const customer = await getCustomerUseCase.execute({
			email: email as string,
			cpf: cpf as string,
		});

		return response.json(customer);
	}
}

export { GetCustomerController };
