import { inject, injectable } from "tsyringe";

import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";
import { ApiError } from "@shared/errors/ApiError";

interface IRequest {
	email: string;
	cpf: string;
}

@injectable()
class GetCustomerUseCase {
	constructor(
		@inject("CustomersRepository")
		private customersRepository: ICustomersRepository
	) {}

	async execute({ email, cpf }: IRequest): Promise<Customer | undefined> {
		if (!email && !cpf) throw new ApiError("Email or Cpf is a required", 400);

		const customer = await this.customersRepository.findByCpfOrEmail(
			email,
			cpf
		);

		if (!customer) throw new ApiError("Customer not exists", 404);

		return customer;
	}
}

export { GetCustomerUseCase };
