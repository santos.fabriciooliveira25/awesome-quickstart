import { Request, Response } from "express";
import { container } from "tsyringe";

import { connect } from "@shared/infra/typeorm";

import { GetCustomerGamesResumeUseCase } from "./GetCustomerGamesResumeUseCase";

class GetCustomerGamesResumeController {
	async handle(request: Request, response: Response): Promise<Response> {
		await connect();

		const { customer_id } = request.params;

		const getGamesResumeUseCase = container.resolve(
			GetCustomerGamesResumeUseCase
		);

		const resume = await getGamesResumeUseCase.execute({
			customer_id,
		});

		return response.json(resume);
	}
}

export { GetCustomerGamesResumeController };
