import { inject, injectable } from "tsyringe";

import { ICustomerGamesRepository } from "@modules/customers/repositories/ICustomerGamesRepository";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";
import { ApiError } from "@shared/errors/ApiError";

interface IRequest {
	customer_id: string;
}

interface IResponse {
	total_games: number;
	total_playtime: number;
	rated_price: number;
}

@injectable()
class GetCustomerGamesResumeUseCase {
	constructor(
		@inject("CustomersRepository")
		private customersRepository: ICustomersRepository,
		@inject("CustomerGamesRepository")
		private customersGamesRepository: ICustomerGamesRepository
	) {}

	async execute({ customer_id }: IRequest): Promise<IResponse> {
		const customerExists = await this.customersRepository.findById(customer_id);

		if (!customerExists) throw new ApiError("Customer not exists", 404);

		const resume = await this.customersGamesRepository.customerGamesResume(
			customer_id
		);

		const response = {
			...resume,
			rated_price: resume.total_games * 250 + resume.total_playtime * 4.5,
		};

		return response;
	}
}

export { GetCustomerGamesResumeUseCase };
