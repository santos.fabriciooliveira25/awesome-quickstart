import { inject, injectable } from "tsyringe";

import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";
import { Parent } from "@modules/customers/infra/typeorm/entities/Parent";
import { ICustomersParentRepository } from "@modules/customers/repositories/ICustomersParentRepository";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";
import { ApiError } from "@shared/errors/ApiError";
import { CustomerStatusEnum } from "@utils/CustomerStatusEnum";

interface IRequest {
	fullname: string;
	cpf: string;
	email: string;
	birthdate: string;
	cellphone: string;
	cep: string;
	parent: Parent;
	status: string;
}

@injectable()
class CreateCustomerUseCase {
	constructor(
		@inject("CustomersRepository")
		private customersRepository: ICustomersRepository,
		@inject("CustomersParentRepository")
		private customerParentRepository: ICustomersParentRepository
	) {}

	async execute({
		fullname,
		cpf,
		email,
		birthdate,
		cellphone,
		cep,
		parent,
		status,
	}: IRequest): Promise<Customer> {
		const customerAlreadyExists = await this.customersRepository.findByCpfOrEmail(
			email
		);

		if (customerAlreadyExists)
			throw new ApiError("Customer already exists", 409);

		if (parent) {
			const createParent = await this.customerParentRepository.create({
				fullname: parent.fullname,
				cpf: parent.cpf,
				email: parent.email,
				birthdate: parent.birthdate,
				cellphone: parent.cellphone,
				cep: parent.cep,
			});

			try {
				const customer = await this.customersRepository.create({
					fullname,
					cpf,
					email,
					birthdate,
					cellphone,
					cep,
					parent: createParent,
					status: status || CustomerStatusEnum.PENDING,
				});

				return customer;
			} catch (e) {
				await this.customerParentRepository.delete(createParent);
				throw new ApiError("Error creating customer", 400);
			}
		}

		const customer = await this.customersRepository.create({
			fullname,
			cpf,
			email,
			birthdate,
			cellphone,
			cep,
			status: status || CustomerStatusEnum.PENDING,
		});

		return customer;
	}
}

export { CreateCustomerUseCase };
