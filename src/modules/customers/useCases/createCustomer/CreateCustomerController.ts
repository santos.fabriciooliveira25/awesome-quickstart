import { Request, Response } from "express";
import { container } from "tsyringe";

import { CreateCustomerUseCase } from "@modules/customers/useCases/createCustomer/CreateCustomerUseCase";
import { connect } from "@shared/infra/typeorm";

class CreateCustomerController {
	async handle(request: Request, response: Response): Promise<Response> {
		await connect();

		const {
			fullname,
			cpf,
			email,
			birthdate,
			cellphone,
			cep,
			parent,
			status,
		} = request.body;

		const createCustomerUseCase = container.resolve(CreateCustomerUseCase);

		const customer = await createCustomerUseCase.execute({
			fullname,
			cpf,
			email,
			birthdate,
			cellphone,
			cep,
			parent,
			status,
		});

		return response.status(201).json(customer);
	}
}

export { CreateCustomerController };
