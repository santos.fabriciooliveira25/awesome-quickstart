import { Request, Response } from "express";
import { container } from "tsyringe";

import { CreateCustomerGamesUseCase } from "@modules/customers/useCases/createCustomerGames/CreateCustomerGamesUseCase";
import { connect } from "@shared/infra/typeorm";

class CreateCustomerGamesController {
	async handle(request: Request, response: Response): Promise<Response> {
		await connect();

		const { customer_id } = request.params;
		const { games } = request.body;

		const createCustomerGamesUseCase = container.resolve(
			CreateCustomerGamesUseCase
		);

		const customer = await createCustomerGamesUseCase.execute({
			customer_id,
			games,
		});

		return response.status(201).json(customer);
	}
}

export { CreateCustomerGamesController };
