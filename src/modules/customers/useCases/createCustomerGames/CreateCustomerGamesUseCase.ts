import { inject, injectable } from "tsyringe";

import { Game } from "@modules/customers/infra/typeorm/entities/Game";
import { ICustomerGamesRepository } from "@modules/customers/repositories/ICustomerGamesRepository";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";
import { ApiError } from "@shared/errors/ApiError";

interface IRequest {
	customer_id: string;
	games: Game[];
}

@injectable()
class CreateCustomerGamesUseCase {
	constructor(
		@inject("CustomersRepository")
		private customersRepository: ICustomersRepository,
		@inject("CustomerGamesRepository")
		private customersGamesRepository: ICustomerGamesRepository
	) {}

	async execute({ customer_id, games }: IRequest): Promise<void> {
		const customerExists = await this.customersRepository.findById(customer_id);

		if (!customerExists) throw new ApiError("Customer not exists", 404);

		games.map(async (game) => {
			await this.customersGamesRepository.create({
				name: game.name,
				playtime: game.playtime,
				background_image: game.background_image,
				store_name: game.store_name,
				store_id: game.store_id,
				store_slug: game.store_slug,
				customer_id,
			});
		});
	}
}

export { CreateCustomerGamesUseCase };
