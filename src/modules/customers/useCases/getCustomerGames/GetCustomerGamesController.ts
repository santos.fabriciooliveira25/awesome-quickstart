import { Request, Response } from "express";
import { container } from "tsyringe";

import { GetCustomerGamesUseCase } from "@modules/customers/useCases/getCustomerGames/GetCustomerGamesUseCase";
import { connect } from "@shared/infra/typeorm";

class GetCustomerGamesController {
	async handle(request: Request, response: Response): Promise<Response> {
		await connect();

		const { customer_id } = request.params;

		const getGamesUseCase = container.resolve(GetCustomerGamesUseCase);

		const customer = await getGamesUseCase.execute({
			customer_id,
		});

		return response.json(customer);
	}
}

export { GetCustomerGamesController };
