import { inject, injectable } from "tsyringe";

import { Game } from "@modules/customers/infra/typeorm/entities/Game";
import { ICustomerGamesRepository } from "@modules/customers/repositories/ICustomerGamesRepository";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";
import { ApiError } from "@shared/errors/ApiError";

interface IRequest {
	customer_id: string;
}

@injectable()
class GetCustomerGamesUseCase {
	constructor(
		@inject("CustomersRepository")
		private customersRepository: ICustomersRepository,
		@inject("CustomerGamesRepository")
		private customersGamesRepository: ICustomerGamesRepository
	) {}

	async execute({ customer_id }: IRequest): Promise<Game[]> {
		const customerExists = await this.customersRepository.findById(customer_id);

		if (!customerExists) throw new ApiError("Customer not exists", 404);

		const games = await this.customersGamesRepository.findGamesByCustomerId(
			customer_id
		);

		return games;
	}
}

export { GetCustomerGamesUseCase };
