import { Request, Response } from "express";
import { container } from "tsyringe";

import { GetGamesUseCase } from "@modules/customers/useCases/getGames/getGamesUseCase";

class GetGamesController {
	async handle(request: Request, response: Response): Promise<Response> {
		const searchTerm = request.query?.searchTerm || "";

		const getGamesUseCase = container.resolve(GetGamesUseCase);

		const games = await getGamesUseCase.execute({
			searchTerm: searchTerm as string,
		});

		return response.json(games);
	}
}

export { GetGamesController };
