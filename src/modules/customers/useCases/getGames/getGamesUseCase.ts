import Axios from "axios";
import queryString from "query-string";

const KEY_RAWG = "f41e56d2046d4bafb113b51d949e82a4";

interface IRequest {
	searchTerm: string;
}

class GetGamesUseCase {
	async execute({ searchTerm }: IRequest): Promise<Response> {
		const { data } = await Axios.get(
			`https://api.rawg.io/api/games?platforms=187%2C1%2C18%2C186%2C7%2C4&${queryString.stringify(
				{
					search: searchTerm,
					page_size: 5,
					page: 1,
					key: KEY_RAWG,
					exclude_additions: true,
				}
			)}`
		);

		return data;
	}
}

export { GetGamesUseCase };
