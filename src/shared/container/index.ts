import { container } from "tsyringe";

import { CustomerGamesRepository } from "@modules/customers/infra/typeorm/repositories/CustomerGamesRepository";
import { CustomersParentRepository } from "@modules/customers/infra/typeorm/repositories/CustomersParentRepository";
import { CustomersRepository } from "@modules/customers/infra/typeorm/repositories/CustomersRepository";
import { ICustomerGamesRepository } from "@modules/customers/repositories/ICustomerGamesRepository";
import { ICustomersParentRepository } from "@modules/customers/repositories/ICustomersParentRepository";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";

container.registerSingleton<ICustomersRepository>(
	"CustomersRepository",
	CustomersRepository
);

container.registerSingleton<ICustomersParentRepository>(
	"CustomersParentRepository",
	CustomersParentRepository
);

container.registerSingleton<ICustomerGamesRepository>(
	"CustomerGamesRepository",
	CustomerGamesRepository
);
