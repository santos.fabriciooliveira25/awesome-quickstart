import express from "express";

import { CreateCustomerController } from "@modules/customers/useCases/createCustomer/CreateCustomerController";
import { CreateCustomerGamesController } from "@modules/customers/useCases/createCustomerGames/CreateCustomerGamesController";
import { GetCustomerController } from "@modules/customers/useCases/getCustomer/getCustomerController";
import { GetCustomerGamesController } from "@modules/customers/useCases/getCustomerGames/GetCustomerGamesController";
import { GetCustomerGamesResumeController } from "@modules/customers/useCases/getCustomerGamesResume/GetCustomerGamesResumeController";
import { GetGamesController } from "@modules/customers/useCases/getGames/getGamesController";
import { InviteCustomerController } from "@modules/customers/useCases/inviteCustomer/InviteCustomerController";
import { UpdateCustomerController } from "@modules/customers/useCases/updateCustomer/UpdateCustomerController";

const customersRoutes = express();

const createCustomerController = new CreateCustomerController();
const inviteCustomerController = new InviteCustomerController();
const getCustomerController = new GetCustomerController();
const updateCustomerController = new UpdateCustomerController();
const getCustomerGamesController = new GetCustomerGamesController();
const createCustomerGamesController = new CreateCustomerGamesController();
const getCustomerGamesResumeController = new GetCustomerGamesResumeController();
const getGamesController = new GetGamesController();

customersRoutes.post("/", createCustomerController.handle);

customersRoutes.post("/invite", inviteCustomerController.handle);

customersRoutes.put("/:id", updateCustomerController.handle);

customersRoutes.get("/find", getCustomerController.handle);

customersRoutes.get("/:customer_id/games", getCustomerGamesController.handle);

customersRoutes.get(
	"/:customer_id/games/resume",
	getCustomerGamesResumeController.handle
);

customersRoutes.post(
	"/:customer_id/games",
	createCustomerGamesController.handle
);

customersRoutes.get("/games", getGamesController.handle);

export { customersRoutes };
