import express from "express";

import { GetGamesController } from "@modules/customers/useCases/getGames/getGamesController";

const fooRoutes = express();

const getGamesController = new GetGamesController();

fooRoutes.get("/games", getGamesController.handle);

export { fooRoutes };
