import server from "../server";
// import { Router } from "express";
// import { ensureAuthenticatedFirebase } from "@shared/infra/http/middlewares/ensureAuthenticatedFirebase";
import { customersRoutes } from "./customers.routes";
import { fooRoutes } from "./foo.routes";

// const router = Router();

const customersApp = server("/customers", customersRoutes);
const fooApp = server("/foo", fooRoutes);

export { customersApp, fooApp };
