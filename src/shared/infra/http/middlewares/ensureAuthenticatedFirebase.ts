import { Request, Response, NextFunction } from "express";
import * as admin from "firebase-admin";

import { ApiError } from "@shared/errors/ApiError";

// if (!process.env.FIREBASE_SERVICE_ACCOUNT)
// 	throw new ApiError("Missing FIREBASE_SERVICE_ACCOUNT environment variable");

// const serviceAccountCredentials = Buffer.from(
// 	process.env.FIREBASE_SERVICE_ACCOUNT,
// 	"base64"
// ).toString("ascii");

// const serviceAccount = JSON.parse(serviceAccountCredentials);

// admin.initializeApp({
// 	credential: admin.credential.cert(serviceAccount),
// });

export async function ensureAuthenticatedFirebase(
	request: Request,
	response: Response,
	next: NextFunction
): Promise<void> {
	const authHeader = request.header("x-auth");

	if (!authHeader) {
		throw new ApiError("Missing token", 401);
	}

	try {
		await admin.auth().verifyIdToken(authHeader, true);

		next();
	} catch (e) {
		throw new ApiError("Invalid token", 401);
	}
}
