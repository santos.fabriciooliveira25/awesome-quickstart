import "dotenv/config";
import "reflect-metadata";
import cors from "cors";
import express, {
	NextFunction,
	Request,
	Response,
	Router,
	Express,
} from "express";
import swaggerUi from "swagger-ui-express";

import { ApiError } from "@shared/errors/ApiError";

import "express-async-errors";
import "@shared/container";

import swaggerFile from "../../../swagger.json";

export default (path: string, router: Router): Express => {
	const app = express();

	app.use(express.json());

	app.use(cors());

	app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerFile));

	app.use(path, router);

	app.use(
		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		(e: Error, request: Request, response: Response, next: NextFunction) => {
			if (e instanceof ApiError) {
				return response.status(e.statusCode).json({
					message: e.message,
				});
			}
			return response.status(500).json({
				status: "error",
				message: `Internal server error - ${e.message}`,
			});
		}
	);

	return app;
};
