import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CustomerGames1622213512976 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: "customer_games",
				columns: [
					{
						name: "id",
						type: "uuid",
						isPrimary: true,
					},
					{
						name: "created_at",
						type: "timestamp",
						default: "now()",
					},
					{
						name: "updated_at",
						type: "timestamp",
						default: "now()",
					},
					{
						name: "name",
						type: "varchar",
					},
					{
						name: "playtime",
						type: "integer",
					},
					{
						name: "background_image",
						type: "varchar",
						isNullable: true,
					},
					{
						name: "store_id",
						type: "varchar",
						isNullable: true,
					},
					{
						name: "store_name",
						type: "varchar",
					},
					{
						name: "store_slug",
						type: "varchar",
						isNullable: true,
					},
				],
			})
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable("customer_games");
	}
}
