import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CustomerParent1622157292342 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: "customers_parent",
				columns: [
					{
						name: "id",
						type: "uuid",
						isPrimary: true,
					},
					{
						name: "created_at",
						type: "timestamp",
						default: "now()",
					},
					{
						name: "updated_at",
						type: "timestamp",
						default: "now()",
					},
					{
						name: "fullname",
						type: "varchar",
					},
					{
						name: "cpf",
						type: "varchar",
					},
					{
						name: "email",
						type: "varchar",
					},
					{
						name: "birthdate",
						type: "date",
					},
					{
						name: "cellphone",
						type: "varchar",
					},
					{
						name: "cep",
						type: "varchar",
					},
				],
			})
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable("customers_parent");
	}
}
