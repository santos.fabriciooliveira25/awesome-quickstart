import {
	MigrationInterface,
	QueryRunner,
	TableColumn,
	TableForeignKey,
} from "typeorm";

export class AlterCustomerGamesAddCustomerId1622214220216
	implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumn(
			"customer_games",
			new TableColumn({
				name: "customer_id",
				type: "uuid",
			})
		);
		await queryRunner.createForeignKey(
			"customer_games",
			new TableForeignKey({
				name: "FKCustomers",
				referencedTableName: "customers",
				referencedColumnNames: ["id"],
				columnNames: ["customer_id"],
				onDelete: "CASCADE",
			})
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropForeignKey("customer_games", "FKCustomers");
		await queryRunner.dropColumn("customer_games", "customer_id");
	}
}
