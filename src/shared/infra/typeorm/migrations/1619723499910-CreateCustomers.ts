import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateCustomers1619723499910 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: "customers",
				columns: [
					{
						name: "id",
						type: "uuid",
						isPrimary: true,
					},
					{
						name: "created_at",
						type: "timestamp",
						default: "now()",
					},
					{
						name: "updated_at",
						type: "timestamp",
						default: "now()",
					},
					{
						name: "fullname",
						type: "varchar",
					},
					{
						name: "cpf",
						type: "varchar",
						isNullable: true,
					},
					{
						name: "email",
						type: "varchar",
						isUnique: true,
					},
					{
						name: "birthdate",
						type: "date",
						isNullable: true,
					},
					{
						name: "cellphone",
						type: "varchar",
					},
					{
						name: "cep",
						type: "varchar",
						isNullable: true,
					},
					{
						name: "status",
						type: "varchar",
						isNullable: true,
					},
				],
			})
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable("customers");
	}
}
