import {
	MigrationInterface,
	QueryRunner,
	TableColumn,
	TableForeignKey,
} from "typeorm";

export class AlterCustomerAddParentId1622173910460
	implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumn(
			"customers",
			new TableColumn({
				name: "parent_id",
				type: "uuid",
				isNullable: true,
			})
		);
		await queryRunner.createForeignKey(
			"customers",
			new TableForeignKey({
				name: "FKCustomersParent",
				referencedTableName: "customers_parent",
				referencedColumnNames: ["id"],
				columnNames: ["parent_id"],
				onDelete: "SET NULL",
			})
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropForeignKey("customers", "FKCustomersParent");
		await queryRunner.dropColumn("customers", "parent_id");
	}
}
