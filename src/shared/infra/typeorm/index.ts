import path from "path";
import {
	ConnectionOptions,
	Connection,
	createConnection,
	getConnection,
} from "typeorm";

const config: ConnectionOptions = {
	name: "default",
	type: "postgres",
	host: process.env.DB_HOST,
	port: Number(process.env.DB_PORT),
	username: process.env.DB_USERNAME,
	password: process.env.DB_PASSWORD,
	database: process.env.DB_NAME,
	synchronize: true,
	logging: false,
	entities: [
		path.join(
			__dirname,
			"../../../modules/**/infra/typeorm/entities/*{.ts,.js}"
		),
	],
};

export const connect = async (): Promise<Connection> => {
	let connection: Connection;

	try {
		connection = getConnection(config.name);
	} catch (err) {
		connection = await createConnection(config);
	}
	return connection;
};
