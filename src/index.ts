import * as functions from "firebase-functions";

import { customersApp, fooApp } from "@shared/infra/http/routes";

export const api_customers = functions.https.onRequest(customersApp);
export const api_foo = functions.https.onRequest(fooApp);
